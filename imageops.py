import cv2
import numpy as np
import math


def crop_image(img, bbox):
    if isinstance(bbox, tuple):
        x1, y1, x2, y2 = bbox[0], bbox[1], bbox[2], bbox[3]
    elif isinstance(bbox, np.ndarray):
        bbox_ints = bbox.astype(dtype='int')
        x1, y1, x2, y2 = bbox_ints[0][0][0], bbox_ints[0][0][1], bbox_ints[0][0][2], bbox_ints[0][0][3]
    else:
        bbox_ints = bbox.asnumpy().astype(dtype='int')
        x1, y1, x2, y2 = bbox_ints[0][0][0], bbox_ints[0][0][1], bbox_ints[0][0][2], bbox_ints[0][0][3]

    crop_img = img[y1:y2, x1:x2]
    return crop_img


def add_padding(img, target_dims):
    img_h, img_w = img.shape[:2]
    target_h, target_w = target_dims
    top = max(math.floor((target_h - img_h) // 2), 0)
    bottom = max(target_h - img_h - top, 0)
    left = max(math.floor((target_w - img_w) // 2), 0)
    right = max(target_w - img_w - left, 0)
    padded = cv2.copyMakeBorder(img, top, bottom, left, right, cv2.BORDER_CONSTANT)
    try:
        assert tuple(padded.shape[:2]) == target_dims
    except AssertionError:
        print("Assertion Error: Padded image and target image shape do not match.")
        print("Padded image shape:", tuple(padded.shape[:2]))
        print("Target dims:", target_dims)
        return cv2.resize(img, (target_w, target_h))
    return padded
