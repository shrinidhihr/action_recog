import cv2
import glob
import os
from pathlib import Path


def load_frames_from_video(input_video_file):
    file_name = Path(input_video_file).stem
    folder = Path(input_video_file).parent
    video_reader = cv2.VideoCapture(input_video_file)
    if not video_reader.isOpened():
        raise IOError("Error opening video file")

    frame = 0
    while True:
        ret, img_bgr = video_reader.read()
        if not ret:
            break
        src = Path(folder, f"{file_name}_frame_{frame}.png")

        output_folder = f"./outputs/"
        if not os.path.exists(output_folder):
            os.mkdir(output_folder)

        dst = Path(f"{output_folder}", folder.name, f"{file_name}_frame_{frame}.png")
        frame += 1
        yield src, dst, img_bgr


def load_image(src):
    return cv2.imread(str(src), cv2.COLOR_BGR2RGB)


def load_frames(input_dir):
    if Path(input_dir).is_file() and input_dir.split('.')[1] in ['jpg', 'png', 'jpeg']:
        return [Path(input_dir)]
    else:
        for filename in glob.iglob(input_dir + '/**', recursive=True):
            if filename.split('.')[-1] in ['jpg', 'png', 'jpeg']:
                src = Path(filename)
                folder = Path(filename).parent.name
                dst = Path(f'./outputs/{folder}/{Path(filename).name}')
                yield src, dst, load_image(src)
            elif filename.split('.')[-1] in ['mp4']:
                for src, dst, img in load_frames_from_video(filename):
                    yield src, dst, img
