import os
import pickle
from pathlib import Path

import mxnet as mx
import numpy as np
import pandas as pd
import sklearn
from predict_skeletons import class_map
if os.name == 'nt':
    import pathlib

    temp = pathlib.PosixPath
    pathlib.PosixPath = pathlib.WindowsPath

import visualize

from process_pickles import get_res_objects
from pathlib import WindowsPath

from imblearn.over_sampling import SMOTE


def get_folder_name(folder):
    if os.name == 'nt':
      splitter = '\\'
    else:
      splitter = '/'
    return str(folder).split(splitter)[-2]

def save_skeletons_image_paths_to_pickle(path_to_grouped_skeletons, path_to_image_paths_pickle):
    # Get all files
    folders = [Path(path_to_grouped_skeletons, folder) for folder in os.listdir(path_to_grouped_skeletons)]

    good_image_paths = {}
    bad_image_paths = {}

    for folder in folders:
        if "_bad" in str(folder):
            for image in os.listdir(folder):
                f = Path(folder, image)
                path_to_image = Path(str(f.parent.name).replace('_bad', ''), f.name)
                key = get_folder_name(path_to_image)
                if key in bad_image_paths:
                    bad_image_paths[key].append(path_to_image)
                else:
                    bad_image_paths[key] = [path_to_image]
        else:
            for image in os.listdir(folder):
                f = Path(folder, image)
                path_to_image = Path(f.parent.name, f.name)
                key = get_folder_name(path_to_image)
                if key in good_image_paths:
                    good_image_paths[key].append(path_to_image)
                else:
                    good_image_paths[key] = [path_to_image]

    with open(path_to_image_paths_pickle, 'ab') as output_file:
        pickle.dump(good_image_paths, output_file)
        pickle.dump(bad_image_paths, output_file)


def load_skeletons_image_paths_from_pickle(path_to_image_paths_pickle):
    with open(path_to_image_paths_pickle, 'rb') as outputFile:
        try:
            good_image_paths = pickle.load(outputFile)
            bad_image_paths = pickle.load(outputFile)
            return good_image_paths, bad_image_paths
        except EOFError as e:
            print(e)


def getColumns():
    cols_keypoints = []
    for k, v in visualize.KEYPOINTS.items():
        cols_keypoints.append(v + '.x')
        cols_keypoints.append(v + '.y')

    cols_bbox = ['bbx1', 'bby1', 'bbx2', 'bby2']
    cols_dimensions = ['height', 'width']
    cols_classes = ['pose']

    cols = cols_keypoints + cols_bbox + cols_dimensions + cols_classes

    return cols, cols_keypoints, cols_bbox, cols_dimensions, cols_classes


def load_dataframe_from_pickle(pickle_file_path, bad_image_paths):
    good_images_map = {}
    cols, cols_keypoints, cols_bbox, cols_dimensions, cols_classes = getColumns()

    for res in get_res_objects(pickle_file_path):
        if not res['skeletonFound']:
            continue

        # Check if skeleton is bad
        f = res['file_name']
        path_to_image = WindowsPath(f.parent.name, f.name)
        key = str(f.parent.name)
        if path_to_image in bad_image_paths[key]:
            continue

        keypoint_coords = res['pred_coords']
        image_dims = res['height'], res['width']
        target_dims = (300, 150,)
        bbox = res['bbox']
        coords, bbox, new_dims = visualize.scaleKeypoints(keypoint_coords, bbox, image_dims, target_dims)

        row = coords.asnumpy().astype(dtype='object').flatten()
        row = np.append(row, bbox)
        row = np.append(row, new_dims[0])
        row = np.append(row, new_dims[1])
        row = np.append(row, get_folder_name(res['file_name']))

        img_class = str(f.parent.name)
        if img_class in good_images_map:
            good_images_map[img_class].append(row)
        else:
            good_images_map[img_class] = [row]
        assert len(cols) == len(row)
    return good_images_map, cols, cols_keypoints, cols_bbox, cols_dimensions, cols_classes


def train_test_split(good_images_map, cols, split_ratio):
    train = pd.DataFrame(columns=cols)
    test = pd.DataFrame(columns=cols)

    for k, v in good_images_map.items():
        split_index = int(split_ratio * len(v))
        train_rows = v[:split_index]
        test_rows = v[split_index:]
        train = train.append(pd.DataFrame(train_rows, columns=cols))
        test = test.append(pd.DataFrame(test_rows, columns=cols))

    x_train = train[cols[:-1]]
    y_train = train[cols[-1]]

    # Use x_test, y_test for testing the model.
    x_test = test[cols[:-1]]
    y_test = test[cols[-1]]
    return x_train, y_train, x_test, y_test


def convertDfDataToSkeletons(featuresDf, targetDf, cols_keypoints, cols_bbox, cols_dimensions):
    images = []
    classes = []
    toSkip = []
    for idx, row in featuresDf.iterrows():
        keypoints, bbox, dimensions = row.loc[cols_keypoints].values.flatten(), row[cols_bbox], row[cols_dimensions]
        image_dims = dimensions[0], dimensions[1]
        bounding_boxs = mx.nd.array([[[bbox['bbx1'], bbox['bby1'], bbox['bbx2'], bbox['bby2']]]])
        keypoints = mx.nd.array([np.array(keypoints).reshape(17, 2)])

        img = visualize.getSkeletonImage(keypoints, bounding_boxs, [], 0, image_dims, (300, 150,))
        if img is None:
            toSkip.append(idx)
            continue
        images.append(img)

    for idx, row in targetDf.iteritems():
        if idx in toSkip:
            continue
        classes.append(class_map[row])

    x_data = np.array(images)
    y_data = np.array(classes)
    assert len(images) == len(classes)
    return x_data, y_data


def load_data_with_smote_sampling():
    # path_to_image_paths_pickle = 'good_image_paths_v2.pickle'
    path_to_image_paths_pickle = 'good_image_paths.pickle'

    # Uncomment the following two lines to make fresh pickles
    # path_to_grouped_skeletons = './outputs/grouped'
    # save_skeletons_image_paths_to_pickle(path_to_grouped_skeletons, path_to_image_paths_pickle)

    _, bad_image_paths = load_skeletons_image_paths_from_pickle(path_to_image_paths_pickle)

    pickleFilePath = './all_refined_cricket_od_crop_after_mxnet.pickle'
    good_images_map, cols, cols_keypoints, cols_bbox, cols_dimensions, cols_classes = load_dataframe_from_pickle(
        pickleFilePath, bad_image_paths)

    x_train, y_train, x_test, y_test = train_test_split(good_images_map, cols, split_ratio=0.9)

    samplesPerClass = 200
    sm = SMOTE(sampling_strategy={'follow_through_pull_shot': samplesPerClass,
                                  'ready': samplesPerClass,
                                  'straight_drive': samplesPerClass,
                                  'follow_through_cover_drive': samplesPerClass,
                                  'pull_shot': samplesPerClass,
                                  'stride': samplesPerClass,
                                  'cover_drive': samplesPerClass,
                                  'follow_through_straight_drive': samplesPerClass})

    x_train, y_train = sm.fit_resample(x_train, y_train)
    x_train, y_train = convertDfDataToSkeletons(x_train, y_train, cols_keypoints, cols_bbox, cols_dimensions)
    x_test, y_test = convertDfDataToSkeletons(x_test, y_test, cols_keypoints, cols_bbox, cols_dimensions)

    x_train, x_val, y_train, y_val = sklearn.model_selection.train_test_split(x_train, y_train, test_size=0.2)
    return x_train, y_train, x_val, y_val, x_test, y_test


# x_train, y_train, x_val, y_val, x_test, y_test = load_data_with_smote_sampling()
