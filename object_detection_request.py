import requests
import cv2
import mxnet as mx

def get_boundary_boxes_batsman_OD(img):
    # Switch between the two when you run locally vs on r2.
    # local
    url = "http://203.92.62.18:6005/object-detection/upload-frame/"
    # url = "http://192.168.0.116:6005/object-detection/upload-frame/"

    payload = {'model_to_use': '0',
               'object_index': '3'}

    cv2.imwrite('temp.png', img)
    files = [
        ('frame', ('temp.png',
                   open('temp.png', 'rb'), 'image/png'))
    ]
    headers = {}

    if "192.168.0.166" in url:
        import sys
        # sys.path.insert(0, "~/sindhu/OD")
        from OD.orchestrator.od_orchestrator import detect_objects_from_frame
        data = detect_objects_from_frame(frame, frame_name, payload['model_to_use'], payload['object_index'], True)
        if data["bounding_boxes"] == list():
            data = None
        response = {"status": True, "predictions": data}
    else:
        response = requests.request("POST", url, headers=headers, data=payload, files=files)
        response = response.json()

    if response['predictions'] is None:
        # print("No batsman found...")
        return None, None, None, None
    batsmen_idxs = [i for i in range(len(response['predictions']['labels'])) if
                    response['predictions']['labels'][i] == 'batsman']

    if batsmen_idxs == list(): # empty list
        return None, None, None, None
    bounding_boxes_batsmen = [response['predictions']['bounding_boxes'][index] for index in batsmen_idxs]
    scores_batsmen = [response['predictions']['scores'][index] for index in batsmen_idxs]
    labels_batsmen = [response['predictions']['labels'][index] for index in batsmen_idxs]

    response['predictions']['bounding_boxes'] = bounding_boxes_batsmen
    response['predictions']['scores'] = scores_batsmen
    response['predictions']['labels'] = labels_batsmen

    class_IDs = mx.nd.array([[[0]]])
    scores = mx.nd.array([[[response['predictions']['scores'][0]]]])

    bbx1 = response['predictions']['bounding_boxes'][0][0]
    bby1 = response['predictions']['bounding_boxes'][0][1]
    bbx2 = response['predictions']['bounding_boxes'][0][2]
    bby2 = response['predictions']['bounding_boxes'][0][3]

    bbx1 = max(bbx1, 0)
    bbx2 = max(bbx2, 0)
    bby1 = max(bby1, 0)
    bby2 = max(bby2, 0)

    bounding_boxs_ints = (bbx1, bby1, bbx2, bby2)
    bounding_boxs_mx_nd_array = mx.nd.array([[[bbx1, bby1, bbx2, bby2]]])

    return class_IDs, scores, bounding_boxs_mx_nd_array, bounding_boxs_ints
