import os

ALLOWED_MISSED_OD = 2

DEFAULT_RETINANET_EPOCHS = 50

DEFAULT_RETINANET_LR = 1e-5

DEFAULT_RETINANET_STEPS = 10000

DEFAULT_RETINANET_BATCH_SIZE = 1

RETINANET_DETECTION_THRESHOLD = 0.01

YOLOV3_DETECTION_THRESHOLD = 0.5

DEFAULT_RETINANET_BACKBONE = "resnet50"

DETECTION_RETINANET = "RetinaNet"

DETECTION_YOLOV3 = "YOLOv3"

NTH_FRAME = 1

IMAGE_FORMAT = ".png"

OUTPUT_DIR = "output"

IMAGE_DIR = "image"

OUTPUT_FPS = 15

MANIFEST_INDEX = ["Fps", "IsAlive", "SegmentLength", "Name"]

NMS_THRESHOLD = 0.1

# YOLOV3_CONFIG_PATH = os.path.abspath(os.path.join("project_data", "annotations", "custom.cfg"))
#
# YOLOV3_WEIGHTS_PATH = os.path.abspath(os.path.join("project_data", "weights", "custom_final.weights"))
#
# YOLOv3_LABEL_PATH = os.path.abspath(os.path.join("project_data", "annotations", "classes-darknet.names"))
#
# IMAGE_FOLDER_PATH = os.path.abspath(os.path.join("project_data", "images"))

FRAME_PATH = os.path.abspath(os.path.join("project_data", "frames"))

MODEL_FOLDER_PATH = os.path.abspath(os.path.join("project_data", "models"))

SNAPSHOT_FOLDER_PATH = os.path.abspath(os.path.join("project_data", "snapshots"))

ANNOTATION_FOLDER_PATH = os.path.abspath(os.path.join("project_data", "annotations"))

CREATE_ANNOTATION_FOLDER_PATH = os.path.abspath(os.path.join("project_data", "annotations"))

WEIGHTS_FOLDER_PATH = os.path.abspath(os.path.join("project_data", "weights"))

PERFORMANCE_TEST_IMAGE_PATH = os.path.abspath(os.path.join("project_data", "images"))
