import os
import sys
import traceback


def exception_formatting(e, logging):
    exc_type, exc_obj, exc_tb = sys.exc_info()
    file = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
    line = exc_tb.tb_lineno
    tb = sys.exc_info()[-1]
    stk = traceback.extract_tb(tb, 1)
    func_name = stk[0][2]
    msg = "Error: " + str(e) + "\n\t\t\t Filename: " + str(file) + "\n\t\t\t Line Number: " + \
          str(line) + " In Function " + str(func_name)
    logging.error(msg)
    return None
