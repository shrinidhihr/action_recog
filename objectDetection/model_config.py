from pathlib import Path

SUPPORTED_MODELS = ["YOLOv3", "RETINA_NET"]
SUPPORTED_TRACKERS = ["csrt", "kcf"]
OBJECT_TYPES = ["basketball", "multiball", "helmet", "cricket"]

PATH_TO_MODEL = Path('/home/thoughtclan/sindhu/OD/realtime_od/project_data')
YOLOv3_CONFIG_PATH = Path("/home/thoughtclan/sindhu/OD/realtime_od/project_data/cricket/annotations","custom.cfg")
YOLOv3_WEIGHTS_PATH = Path("/home/thoughtclan/sindhu/OD/realtime_od/project_data/cricket/weights","custom_final.weights")
YOLOv3_LABEL_PATH = Path("/home/thoughtclan/sindhu/OD/realtime_od/project_data/cricket/annotations","classes-darknet.names")
