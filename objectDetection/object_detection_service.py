import model_config

from darknet_detection import Yolov3

def get_batsman_od_yolo(img):
    cfg_path = model_config.YOLOv3_CONFIG_PATH
    weights_path = model_config.YOLOv3_WEIGHTS_PATH
    label_path = model_config.YOLOv3_LABEL_PATH

    # Model object generation.
    model = Yolov3(cfg_path, weights_path, label_path)
    detection = model.image_predict(img, frame_name)
    data = detect_objects_from_frame(frame, frame_name, payload['model_to_use'], payload['object_index'], True)
