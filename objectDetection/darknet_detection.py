import numpy as np
import cv2
import os
import logging
import mxnet as mx

from .exception import exception_formatting
from .properties import YOLOV3_DETECTION_THRESHOLD, FRAME_PATH, NMS_THRESHOLD

logging.basicConfig(filename='darknet-log.log',
                    filemode='a',
                    format='%(asctime)s - %(levelname)s : %(message)s',
                    datefmt='%m/%d/%Y %I:%M:%S %p',
                    level=logging.DEBUG)


class Yolov3:

    def __init__(self, config_path, weights_path, label_path):
        try:
            self.config_path = config_path
            self.weight_path = weights_path
            self.label_path = label_path
            self.model = cv2.dnn.readNetFromDarknet(str(config_path), str(weights_path))
            self.label_dict = self.label_dictionary()
        except Exception as e:
            exception_formatting(e, logging)

    def label_dictionary(self):
        """

        :return: Returns labels mapping class numbers to their corresponding names
        """
        try:
            label_dict = dict()
            with open(self.label_path, "r") as class_file:
                index = 0
                for class_name in class_file:
                    label_dict[index] = class_name.split()[0]
                    index += 1
            return label_dict

        except Exception as e:
            exception_formatting(e, logging)

    def save_img(self, img, image_name):
        """
        :param img: numpy array to be saved
        :param image_name: name of the image which will be saved
        :return: True if the save succeeded, False otherwise
        """
        if not os.path.exists(os.path.abspath(os.path.join("..", "..", "project_data", "results"))):
            os.mkdir(os.path.abspath(os.path.join("..", "..", "project_data", "results")))
        save_path = os.path.abspath(os.path.join("..", "..", "project_data", "results", image_name))
        cv2.imwrite(save_path, img)
        if os.path.exists(save_path):
            return True
        else:
            return False

    def image_predict(self, image):
        """

        :param image: Image frame as a numpy ndarray which is obtained from the read_video_input() function
        :return: class_ids, scores, classnames and bounding box coordinates for the frame
        """
        try:
            net = self.model
            # print("model ", net)
            # print("image", image.shape)
            (H, W) = image.shape[:2]
            ln = net.getLayerNames()
            ln = [ln[i[0] - 1] for i in net.getUnconnectedOutLayers()]
            blob = cv2.dnn.blobFromImage(image, 1 / 255.0, (416, 416), swapRB=True, crop=False)
            net.setInput(blob)
            layer_outputs = net.forward(ln)
            boxes = []
            confidences = []
            class_ids = []
            for output in layer_outputs:
                for detection in output:
                    scores = detection[5:]
                    class_id = np.argmax(scores)
                    confidence = scores[class_id]
                    # filter out weak predictions
                    if confidence > YOLOV3_DETECTION_THRESHOLD:
                        # YOLO returns the center (x, y)-coordinates of the bounding box followed by the
                        # boxes' width and height
                        box = detection[0:4] * np.array([W, H, W, H])
                        (centerX, centerY, width, height) = box.astype("int")
                        x = int(centerX - (width / 2))
                        y = int(centerY - (height / 2))
                        boxes.append([x, y, x+int(width), y+int(height)])
                        confidences.append(float(confidence))
                        class_ids.append(self.label_dict[class_id])

            batsmen_idxs = [i for i in range(len(class_ids)) if
                            class_ids[i] == 'batsman']
            if boxes == [] or batsmen_idxs == []:
                return None, None, None, None

            bounding_boxes_batsmen = [boxes[index] for index in batsmen_idxs]
            scores_batsmen = [confidences[index] for index in batsmen_idxs]
            # labels_batsmen = [class_ids[index] for index in batsmen_idxs]


            class_IDs = mx.nd.array([[[0]]])
            scores = mx.nd.array([[[scores_batsmen[0]]]])

            bbx1 = bounding_boxes_batsmen[0][0]
            bby1 = bounding_boxes_batsmen[0][1]
            bbx2 = bounding_boxes_batsmen[0][2]
            bby2 = bounding_boxes_batsmen[0][3]

            bbx1 = max(bbx1, 0)
            bbx2 = max(bbx2, 0)
            bby1 = max(bby1, 0)
            bby2 = max(bby2, 0)

            bounding_boxs_ints = (bbx1, bby1, bbx2, bby2)
            bounding_boxs_mx_nd_array = mx.nd.array([[[bbx1, bby1, bbx2, bby2]]])

            # confidence default=0.5
            # threshold default=0.3,
            return class_IDs, scores, bounding_boxs_mx_nd_array, bounding_boxs_ints

        except Exception as e:
            print(e)
            exception_formatting(e, logging)

    def save_output_image(self, image, prediction, threshold):
        """

        :param image: Image read from cv2
        :param prediction: Output dictionary of values
        :param threshold: YOLOv3_DETECTION_THRESHOLD
        :return: Saves image with predictions
        """
        try:
            boxes = prediction["bounding_boxes"]
            confidences = prediction["scores"]
            class_labels = prediction["labels"]
            image_name = prediction["image_name"]
            idxs = cv2.dnn.NMSBoxes(boxes, confidences, threshold, NMS_THRESHOLD)
            if len(idxs) > 0:
                for i in idxs.flatten():
                    (x, y) = (boxes[i][0], boxes[i][1])
                    (w, h) = (boxes[i][2], boxes[i][3])
                    # draw a bounding box rectangle and label on the image
                    color = (255, 0, 0)
                    cv2.rectangle(image, (x, y), (x + w, y + h), color, 2)
                    text = "{}".format(class_labels[i], confidences[i])
                    cv2.putText(image, text, (x + 15, y - 10), cv2.FONT_HERSHEY_SIMPLEX,
                                1, color, 3)
                    im_path = os.path.abspath(os.path.join(FRAME_PATH, image_name))
                    cv2.imwrite(im_path, image)

        except Exception as e:
            exception_formatting(e, logging)
