# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
This repository contains a pipeline for

1. loading images in a dataset/extracting frames from video 
2. object detection/pose estimation using mxnet
3. drawing skeletons 

This is a minimalistic implementation of the thoughtclan video analysis platform.