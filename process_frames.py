import os
import pickle

import numpy as np
import tensorflow as tf
import mxnet as mx
from gluoncv import model_zoo
from gluoncv.data.transforms.pose import detector_to_alpha_pose, heatmap_to_coord_alpha_pose
from tqdm import tqdm
from visualize import getSkeletonImage
from load_data import load_frames


def expand_bbox(bbox, expand):
    expand_vec = mx.nd.array([[[-expand, -expand, expand, expand]]])
    return bbox + expand_vec


class ImageApp:
    def __init__(self, output_file):
        # Alpha Pose
        self.pose_net = model_zoo.get_model('alpha_pose_resnet101_v1b_coco', pretrained=True)
        self.image_dim = (150, 300)
        self.output_file = output_file

        # Yolo on r2
        from objectDetection import model_config
        from objectDetection.darknet_detection import Yolov3
        cfg_path = model_config.YOLOv3_CONFIG_PATH
        weights_path = model_config.YOLOv3_WEIGHTS_PATH
        label_path = model_config.YOLOv3_LABEL_PATH
        self.od_model = Yolov3(cfg_path, weights_path, label_path)

        # CNN
        self.cnn_model = tf.keras.models.load_model('./smote_200.h5')

    def save(self, res):
        with open(self.output_file, 'ab') as output_file:
            pickle.dump(res, output_file)

    def process_frame(self, img, expand):
        height, width, _ = img.shape
        class_IDs, scores, bounding_boxs_mx_nd_array, bounding_boxs_ints = self.od_model.image_predict(img)

        if class_IDs is None:
            return None

        bounding_boxs = expand_bbox(bounding_boxs_mx_nd_array, expand)
        pose_input, upscale_bbox = detector_to_alpha_pose(img, class_IDs, scores, bounding_boxs)

        predicted_heatmap = self.pose_net(pose_input)
        pred_coords, confidence = heatmap_to_coord_alpha_pose(predicted_heatmap, upscale_bbox)

        keypoint_threshold = 0
        target_dimensions = (300, 150)
        img = getSkeletonImage(pred_coords, bounding_boxs,
                               confidence, keypoint_threshold,
                               (height, width,), target_dimensions)
        prediction_input = np.expand_dims(img, axis=0)

        cnn_prediction = self.cnn_model.predict(prediction_input)
        cnn_class = cnn_prediction.argmax(axis=-1)
        cnn_pred_confidence = np.max(cnn_prediction)

        res = {
            "expand": expand,
            "pred_coords": pred_coords,
            "confidence": confidence,
            "class_IDs": class_IDs,
            "bbox": bounding_boxs_mx_nd_array,
            "bbox_ints": bounding_boxs_ints,
            "scores": scores,
            "cnn_class": cnn_class,
            "cnn_pred_confidence": cnn_pred_confidence
        }

        return res

    def run(self, input_dir, expand=0):
        total = 0
        no_batsman_found = 0
        for src, dst, img in tqdm(load_frames(input_dir), position=0, leave=True, desc="Processing frames"):
            total += 1
            res = self.process_frame(img, expand)
            if res is None:
                no_batsman_found += 1
                res = {'skeletonFound': False}
            else:
                res['skeletonFound'] = True
            res['file_name'] = src
            res['dst'] = dst
            res['height'], res['width'], _ = img.shape
            self.save(res)

        print(f"Processed {total} frames. Did not find any batsman in {no_batsman_found} images.")


if __name__ == '__main__':
    output_file_path = './clips.pickle'
    if os.path.exists(output_file_path):
        os.remove(output_file_path)
    app = ImageApp(output_file=output_file_path)
    #app.run('/home/thoughtclan/ds101/all_refined_cricket_v2.4/',expand=0)
    app.run('/home/thoughtclan/ds101/clips/cover_drive/',expand=0)
    if os.path.exists('temp.png'):
        os.remove('temp.png')
