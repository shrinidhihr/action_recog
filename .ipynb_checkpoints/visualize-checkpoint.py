import math

import cv2
import numpy as np

import imageops

KEYPOINTS = {
    0: "nose",
    1: "leftEye",
    2: "rightEye",
    3: "leftEar",
    4: "rightEar",
    5: "leftShoulder",
    6: "rightShoulder",
    7: "leftElbow",
    8: "rightElbow",
    9: "leftWrist",
    10: "rightWrist",
    11: "leftHip",
    12: "rightHip",
    13: "leftKnee",
    14: "rightKnee",
    15: "leftAnkle",
    16: "rightAnkle"
}

keypoints = {
    'nose': 0,
    'leftEye': 1,
    'rightEye': 2,
    'leftEar': 3,
    'rightEar': 4,
    'leftShoulder': 5,
    'rightShoulder': 6,
    'leftElbow': 7,
    'rightElbow': 8,
    'leftWrist': 9,
    'rightWrist': 10,
    'leftHip': 11,
    'rightHip': 12,
    'leftKnee': 13,
    'rightKnee': 14,
    'leftAnkle': 15,
    'rightAnkle': 16
}

joints = [
    ['leftShoulder', 'rightShoulder'],
    ['leftShoulder', 'leftElbow'],
    ['leftElbow', 'leftWrist'],
    ['rightShoulder', 'rightElbow'],
    ['rightElbow', 'rightWrist'],
    ['leftShoulder', 'leftHip'],
    ['rightShoulder', 'rightHip'],
    ['leftHip', 'rightHip'],
    ['leftHip', 'leftKnee'],
    ['rightHip', 'rightKnee'],
    ['leftKnee', 'leftAnkle'],
    ['rightKnee', 'rightAnkle']
]


def get_joint_pairs():
    joint_pairs = []
    for pair in joints:
        joint = [keypoints[pair[0]], keypoints[pair[1]]]
        joint_pairs.append(joint)
    return joint_pairs


def scaleKeypoints(pred_coords, bbox, image_dims, target_dims):
    if isinstance(pred_coords, list):
        pred_coords = np.asarray(pred_coords)

    bbox_ints = bbox.asnumpy().astype(dtype='int')
    x1, y1, x2, y2 = bbox_ints[0][0][0], bbox_ints[0][0][1], bbox_ints[0][0][2], bbox_ints[0][0][3]
    img_h, img_w = y2 - y1, x2 - x1
    target_h, target_w = target_dims
    scaling_factor = min((target_h / img_h), (target_w / img_w))

    scaled_coordinates = pred_coords * scaling_factor

    bbox = np.round(bbox_ints * scaling_factor)
    bbox = np.floor(bbox)
    bbox = bbox.astype(dtype='int')

    new_dims = (int(math.floor(image_dims[0] * scaling_factor)), int(math.floor(image_dims[1] * scaling_factor)))

    return scaled_coordinates, bbox, new_dims


def plot_skeleton(image_dims, coordinates, confidence, keypoint_thresh, bbox=None):
    height, width = image_dims
    img = np.zeros([height, width, 3], dtype=np.uint8)

    # Draw bbox
    if bbox is not None:
        x1, y1, x2, y2 = bbox[0][0][0], bbox[0][0][1], bbox[0][0][2], bbox[0][0][3]
        cv2.line(img, (x1, y1), (x2, y1), (255, 255, 255), 2)
        cv2.line(img, (x1, y1), (x1, y2), (255, 255, 255), 2)
        cv2.line(img, (x2, y1), (x2, y2), (255, 255, 255), 2)
        cv2.line(img, (x1, y2), (x2, y2), (255, 255, 255), 2)

    if confidence is not None and keypoint_thresh > 0:
        joint_visible = confidence > keypoint_thresh
    else:
        joint_visible = None
    coordinates = coordinates[0].asnumpy().tolist()
    line_pairs = get_joint_pairs()
    for pair in line_pairs:
        if joint_visible is not None and not (joint_visible[0][pair[0]] and joint_visible[0][pair[1]]):
            continue
        x1, y1, x2, y2 = int(coordinates[pair[0]][0]), int(coordinates[pair[0]][1]), int(coordinates[pair[1]][0]), int(
            coordinates[pair[1]][1])
        cv2.circle(img, (int(x1), int(y1)), 3, (255, 255, 255), 2)
        cv2.circle(img, (int(x2), int(y2)), 3, (255, 255, 255), 2)
        cv2.line(img, (x1, y1), (x2, y2), (255, 255, 255), 5)
    return img


def getSkeletonImage(pred_coords, bbox, confidence, keypoint_threshold, image_dims, target_dims, ):
    coords, bbox, new_dims = scaleKeypoints(pred_coords, bbox, image_dims, target_dims)
    img = plot_skeleton(new_dims, coords, confidence, keypoint_threshold, bbox=None)
    cropped_img = imageops.crop_image(img, bbox)
    padded_img = imageops.add_padding(cropped_img, target_dims)
    return padded_img
