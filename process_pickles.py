import os
import pickle
from pathlib import Path
import numpy as np

import cv2

from visualize import getSkeletonImage
# from predict_skeletons import class_map

import predict_skeletons

# class_map = predict_skeletons.class_map

if os.name == 'nt':
    import pathlib

    temp = pathlib.PosixPath
    pathlib.PosixPath = pathlib.WindowsPath


def get_folder_name(file_name):
    if os.name == 'nt':
        try:
            folder_name = str(file_name).split('\\')[-2]
        except IndexError:
            folder_name = str(file_name).split('\\')[-1]
    else:
        try:
            folder_name = str(file_name).split('/')[-2]
        except IndexError:
            folder_name = str(file_name).split('/')[-1]
    return folder_name


def save_skeleton_image(img, file_name, expand, output_dir):
    f_name, extension = file_name.stem, file_name.suffix

    if extension == '':
        extension = '.png'

    # name = f_name + f"_expand_{expand}" + extension
    name = f_name + extension
    folder_name = get_folder_name(file_name)

    output_file_name = Path(output_dir, folder_name, name)
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)

    if not os.path.exists(Path(output_dir, folder_name)):
        os.mkdir(Path(output_dir, folder_name))

    cv2.imwrite(str(output_file_name), img)


def get_res_objects(pickle_file):
    with open(pickle_file, 'rb') as outputFile:
        while True:
            try:
                res = pickle.load(outputFile)
                yield res
            except EOFError:
                break


def draw_skeletons_from_pickle(pickle_file, output_dir, target_dims, keypoint_threshold):
    for res in get_res_objects(pickle_file):
        if not res['skeletonFound']:
            continue
        image_dims = res['height'], res['width']
        img = getSkeletonImage(res['pred_coords'], res['bbox'],
                               res['confidence'], keypoint_threshold,
                               image_dims, target_dims)
        save_skeleton_image(img, res['file_name'], res['expand'], output_dir)


def load_skeletons_from_pickle(pickle_file, target_dims, keypoint_threshold):
    """
    NOTE that this returns x_data, y_data and y_data will not
    make sense if your input is a video file, because y_data is basically list of folders.
    """
    images = []
    classes = []

    for res in get_res_objects(pickle_file):
        if not res['skeletonFound']:
            continue
        image_dims = res['height'], res['width']
        img = getSkeletonImage(res['pred_coords'], res['bbox'],
                               res['confidence'], keypoint_threshold,
                               image_dims, target_dims)
        images.append(img)
        # classes.append(class_map[get_folder_name(res['file_name'])])
        classes.append(res['file_name'])

    x_data = np.array(images)
    y_data = np.array(classes)
    assert len(images) == len(classes)
    return x_data, y_data


if __name__ == '__main__':
    pickle_file = Path('./all_refined_cricket_od_crop_after_mxnet.pickle')
    output_dir = Path('./outputs/exp')
    target_dims = (300, 150,)
    keypoint_threshold = 0

    for res in get_res_objects(pickle_file):
        if not res['skeletonFound']:
            continue
        image_dims = res['height'], res['width']
        img = getSkeletonImage(res['pred_coords'], res['bbox'],
                            res['confidence'], keypoint_threshold,
                            image_dims, target_dims)
        for k,v in res.items():
            print(k)
            print(repr(v))
            print(type(v))
        save_skeleton_image(img, res['file_name'], res['expand'], output_dir)
        break

    # draw_skeletons_from_pickle(pickle_file, output_dir, target_dims, keypoint_threshold)
    #
    # x_data, y_data = load_skeletons_from_pickle(pickle_file, target_dims, keypoint_threshold)

