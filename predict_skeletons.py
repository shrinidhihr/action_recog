import cv2
import numpy as np
import tensorflow as tf
from tqdm import tqdm
import process_pickles
from pathlib import Path

class_map = {
    'ready': 0,
    'straight_drive': 1,
    'cover_drive': 2,
    'pull_shot': 3,
    'follow_through_cover_drive': 4,
    'follow_through_straight_drive': 5,
    'follow_through_pull_shot': 6,
    'stride': 7
}

reverse_map = {}
for k, v in class_map.items():
    reverse_map[v] = k


def predict_class(img, model):
    # Predict the class of the shot played using the trained model
    fin_img = np.expand_dims(img, axis=0)
    prediction = model.predict(fin_img)
    print("Prediction:",prediction, type(prediction))
    y_classes = prediction.argmax(axis=-1)
    print(y_classes)
    return y_classes[0]


def predict_class_batch(x_data, model):
    preds = model.predict(x_data)
    preds = [(prediction.argmax(axis=-1), np.max(prediction)) for prediction in preds]
    return preds


if __name__ == '__main__':
    model = tf.keras.models.load_model('./smote_200_balanced_classes_march_18.h5')
    pickle_file = './clips_smote_200.pickle'
    x_data, y_data = process_pickles.load_skeletons_from_pickle(pickle_file, target_dims=(300, 150), keypoint_threshold=0)
    predictions = predict_class_batch(x_data, model)
    i = 0
    for prediction, confidence in predictions:
        # print(Path(y_data[i]).name, reverse_map[prediction], confidence)
        print(reverse_map[prediction], confidence)
        i += 1
